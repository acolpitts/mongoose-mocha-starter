const assert = require('assert');
const User = require('../src/user');

describe('Delete User', (done) => {
  let john;

  beforeEach((done) => {
    john = new User({name: 'John Smith'});
    john.save()
      .then(() => done())
  });

  it('should delete a user by calling instance.remove()', (done) => {
    john.remove()
      .then(() => User.findOne({ name: 'John Smith' }))
      .then((user) => {
        assert(user === null);
        done();
      });
  });

  it('should delete a user by calling User.remove()', (done) => {
    User.remove({ name: 'John Smith'})
      .then(() => User.findOne({ name: 'John Smith' }))
      .then((user) => {
        assert(user === null);
        done();
      });
  });

  it('should delete a user by calling User.findAndRemove()', (done) => {
    User.findOneAndRemove({ name: 'John Smith'})
      .then(() => User.findOne({ name: 'John Smith' }))
      .then((user) => {
        assert(user === null);
        done();
      });
  });

  it('should delete a user by calling User.findByIdAndRemove()', (done) => {
    User.findByIdAndRemove({ _id: john._id})
      .then(() => User.findOne({ _id: john._id }))
      .then((user) => {
        assert(user === null);
        done();
      });
  });
});