const assert = require('assert');
const User = require('../src/user');

describe('Update User', (done) => {
  let john;

  beforeEach((done) => {
    john = new User({name: 'John Smith', badgeCount: 0});
    john.save()
      .then(() => done())
  });

  const assertName = (operation, done) => {
    operation
      .then(() => User.find({}))
      .then((users) => {
        assert(users.length === 1);
        assert(users[0].name === 'Jane Smith');
        done();
      })
  };

  it('should update an instance using instance.set() and instance.save()', (done) => {
    john.set('name', 'Jane Smith');
    assertName(john.save(), done)
  });

  it('should update a user by calling instance.update()', (done) => {
    assertName(john.update({ name: 'Jane Smith' }), done);
  });

  it('should update a user by calling User.update()', (done) => {
    assertName(
      User.update({ name: 'John Smith' }, { name: 'Jane Smith' }),
      done
    );
  });

  it('should update a single user by calling User.findOneAndUpdate()', (done) => {
    assertName(
      User.findOneAndUpdate({ name: 'John Smith' }, { name: 'Jane Smith' }),
      done
    );
  });

  it('should update a user by _id calling User.findByIdAndUpdate()', (done) => {
    assertName(
      User.findByIdAndUpdate({ _id: john._id }, { name: 'Jane Smith' }),
      done
    )
  });

  it('should increment all users badgeCount by 1', (done) => {
    User.updateMany({ name: 'John Smith' }, { $inc: { badgeCount: 1 } })
      .then(() => User.findOne({ name: 'John Smith' }))
      .then((user) => {
        assert(user.badgeCount === 1);
        done();
      })
  });

});