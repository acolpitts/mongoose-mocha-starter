const mongoose = require('mongoose');
const MONGODB_TEST_URI = 'mongodb://localhost/users_test';
mongoose.Promise = global.Promise;

/**
 * Before running any tests, connect to mongodb
 */
before((done) => {
  mongoose.connect(MONGODB_TEST_URI);
  mongoose.connection
    .once('open', () => { done(); })
    .on('error', (error) => {
      console.warn('Warning', error);
    });
});

/**
 * Before running each test, drop all collections
 */
beforeEach((done) => {
  const { users, comments, blogposts } = mongoose.connection.collections;
  users.drop(() => {
    comments.drop(() => {
      blogposts.drop(() => {
        done();
      })
    })
  });
});
