const assert = require('assert');
const User = require('../src/user');

describe('Read User', () => {
  let john, maria, alex, zach;

  beforeEach((done) => {
    alex = new User({ name: 'Alex' });
    john = new User({ name: 'John'});
    maria = new User({ name: 'Maria' });
    zach = new User({ name: 'Zach' });

    Promise.all([john.save(), alex.save(), maria.save(), zach.save()])
      .then(() => done());
  });

  it('should find all users by name', (done) => {
    User.find({name: 'John'})
      .then((users) => {
        assert(users[0]._id.toString() === john._id.toString());
        done();
      })
  });

  it('should find a user by id', (done) => {
    User.findOne({_id: john.id.toString()})
      .then((user) => {
        assert(user.name === 'John');
        done();
      });

  });

  it('should skip and limit a result set', (done) => {
    User.find({})
      .sort({ name: 1 })
      .skip(1)
      .limit(2)
      .then((users) => {
        assert(users.length === 2);
        assert(users[0].name === 'John');
        assert(users[1].name === 'Maria');
        done();
      })
      .catch((err) => {
        console.log(err);
      })

  })
});