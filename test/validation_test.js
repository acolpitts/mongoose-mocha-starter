const assert = require('assert');
const User = require('../src/user');

describe('Validating Records', () => {
  it('should require a name', () => {
    const user = new User({ name: '' });
    const validationResult = user.validateSync();
    const { message } = validationResult.errors.name;

    assert(message === 'Name is required.');
  });

  it('should require a name that is at least 2 characters', () => {
    const user = new User({ name: 'L' });
    const validationResult = user.validateSync();
    const { message } = validationResult.errors.name;

    assert(message === 'Name must be at least 2 characters.');
  });

  it('should not allow invalid records to be saved', (done) => {
    const user = new User({ name: 'L' });
    user.save()
      .catch((validationResult) => {
        const { message } = validationResult.errors.name;
        assert(message === 'Name must be at least 2 characters.');
        done();
      })

  });

});
