const assert = require('assert');
const User = require('../src/user');

describe('Create User', () => {
  it('should create a new user', (done) => {
    const user = new User({name: 'Test'})
    user.save()
      .then(() => {
        // Record was successfully saved
        assert(!user.isNew);
        done();

      })
  })
});
