const assert = require('assert');
const User = require('../src/user');

describe('Virtual Types', () => {
  it('should set postCount based on the number of posts',(done) => {
    const user = new User({
      name: 'Test',
      posts: [{title: 'Test 1'}, {title: 'Test 2'}, {title: 'Test 3'}]
    });
    user.save()
      .then(() => User.findOne({ name: 'Test' }))
      .then((result) => {
        assert(result.postCount === 3);
        done();
      })
      .catch((err) => {
        return err;
      })
  })
});