const mongoose = require('mongoose');
const assert = require('assert');
const User = require('../src/user');
const Comment = require('../src/comment');
const BlogPost = require('../src/blogPost');

describe('Associations', () => {
  let user, blogPost, comment;

  beforeEach((done) => {
    user = new User({ name: 'Test'});
    blogPost = new BlogPost({ title: 'JS is Great', content: 'Yep it really is' });
    comment = new Comment({ content: 'Congrats on the great post' });

    // Associate user with blog post
    // A user hasMany blogPosts, so we can use push()
    user.blogPosts.push(blogPost);
    // hasMany
    blogPost.comments.push(comment);
    // hasOne
    // a Comment only hasOne user, mongoose uses setter to grab _id
    comment.user = user;

    Promise.all([user.save(), blogPost.save(), comment.save()])
      .then(() => done());

  });


  it('should save a relation between a user and a blogpost', (done) => {
    User.findOne({ name: 'Test' })
      .populate('blogPosts')
      .then((user) => {
        assert(user.blogPosts[0].title === 'JS is Great');
        done();
      })
  })

  it('should save a deeply nested relationship', (done) => {
    /**
     * Be very careful with populate method as it can be a performance hog
     * when you have large collections and deeply nested relationships
     */
    User.findOne({ name: 'Test' })
      .populate({
        path: 'blogPosts',
        populate: {
          path: 'comments',
          model: 'Comment',
          populate: {
            path: 'user',
            model: 'User'
          }
        }
      })
      .then((user) => {
        assert(user.name = 'Test');
        assert(user.blogPosts[0].title === 'JS is Great');
        assert(user.blogPosts[0].comments[0].content === 'Congrats on the great post');
        assert(user.blogPosts[0].comments[0].user.name === user.name);

        done();
      })
  })
});