const mongoose = require('mongoose');
const assert = require('assert');
const User = require('../src/user');
const BlogPost = require('../src/blogPost');


describe('Middleware', () => {
  let user, blogPost;

  beforeEach((done) => {
    user = new User({ name: 'Test'});
    blogPost = new BlogPost({ title: 'JS is Great', content: 'Yep it really is' });

    // Associate user with blog post
    // A user hasMany blogPosts, so we can use push()
    user.blogPosts.push(blogPost);


    Promise.all([user.save(), blogPost.save()])
      .then(() => done());

  });

  it('should cleanup associated blogposts when removing users', (done) => {
    user.remove()
      .then(() => BlogPost.count())
      .then((count) => {
        assert(count === 0);
        done();
      })
  });

});