const assert = require('assert');
const User = require('../src/user');

describe('Subdocuments', () => {
  it('should create a subdocument', (done) => {
    const user = new User({
      name: 'Test',
      posts: [{ title: 'My test post!' }]
    });

    user.save()
      .then(() => User.findOne({ name: 'Test' }))
      .then((result) => {
        assert(result.posts[0].title === 'My test post!');
        done();
      })

  });

  it('should add a subdocument to an existing user', (done) => {
    const user = new User({
      name: 'Test',
      posts: []
    });

    user.save()
      .then(() => User.findOne({ name: 'Test' }))
      .then((result) => {
        result.posts.push({ title: 'Add Post!'});
        return result.save();
      })
      .then((result) => {
        assert(result.posts[0].title === 'Add Post!')
        done();
      })
  });

  it('should remove a subdocument to an existing user', (done) => {
    const user = new User({
      name: 'Test',
      posts: [{title: 'First Post'}]
    });

    user.save()
      .then(() => User.findOne({ name: 'Test' }))
      .then((result) => {
        result.posts[0].remove();
        return result.save();
      })
      .then(() => User.findOne({ name: 'Test' }))
      .then((result) => {
        assert(result.posts.length === 0);
        done();
      })
  })


});